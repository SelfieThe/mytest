import React from 'react';
import './App.css';
import UserTable from './Pages/UserTable/UserTable';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';

function App() {  
  return (
    <Box
      component="main"
      sx={{
        flexGrow: 1,
        py: 8,
      }}
    >
      <Container maxWidth={false}>
        <Grid
          container
          spacing={3}
        >
          <UserTable />

        </Grid>
      </Container>
    </Box>
  );
}

export default App;
