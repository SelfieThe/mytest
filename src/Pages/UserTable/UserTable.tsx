import * as React from 'react';
import Box from '@mui/material/Box';
import { DataGrid, GridColDef, GridLinkOperator, GridRowParams, GridSelectionModel } from '@mui/x-data-grid';
import { Grid } from '@mui/material';
import { Controller } from './Controller/Controller';
import { ToolBar } from './Components/ToolBar';
import { EditUser } from './Controller/Controller';
import { CreateUser } from './Controller/Controller';
import { DeleteUser } from './Controller/Controller';
import { TableUsers } from './Components/TableUsers';

const columns: GridColDef[] = [
  { field: 'id', headerName: 'ID', width: 90 },
  {
    field: 'UserName',
    headerName: 'UserName',
    width: 150,
    editable: true,
  },
];


export default function UserTable() {
  const ControllerUser = Controller().ControllerUserName
  const [data, setData] = React.useState<{ id: string, UserName: string }[]>([])
  const [selectionRows, setSelectionRows] = React.useState<GridSelectionModel>([]);

  const handleEditUser = EditUser(data)
  const handleCreateUser = CreateUser(ControllerUser, setData, data)
  const handleDeleteUser = DeleteUser(selectionRows, data, setData);
  
  const { dataTable, columns } = TableUsers(data);

  React.useEffect(() => {
    if (data.length > 0) {
      localStorage.setItem('Table', JSON.stringify(data));
    }
  }, [data]);
  
  React.useEffect(() => {
    const items = JSON.parse(localStorage.getItem('Table') || '[]')
    if (items) {
      setData(items);
    }
  }, []);
  
  return (
    <Box sx={{ height: 700, width: '100%' }}>
      <Grid
        item
        xl={12}
        lg={12}
        sm={12}
        xs={12}
      >
        {ToolBar(ControllerUser, handleCreateUser, handleDeleteUser)}
      </Grid>

      <DataGrid
        {...dataTable}
        columns={columns}
        initialState={{
          filter: {
            filterModel: {
              items: [],
              quickFilterLogicOperator: GridLinkOperator.Or,
            },
          },
        }}
        pageSize={25}
        onCellEditCommit={handleEditUser}
        checkboxSelection
        onSelectionModelChange={(newSelectionRow) => {
          setSelectionRows(newSelectionRow);
        }}
        selectionModel={selectionRows}
      />
    </Box>
  )
}