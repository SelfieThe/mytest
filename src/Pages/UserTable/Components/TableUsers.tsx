import * as React from 'react';

export function TableUsers(data: { id: string; UserName: string; }[]) {
  const VISIBLE_FIELDS = ['UserName'];
  const dataTable = {
    dataSet: 'Employee',
    visibleFields: VISIBLE_FIELDS,
    rowLength: 100,
    columns: [
      { field: 'id', headerName: 'ID', width: 50 },
      { field: 'UserName', headerName: 'Имя', width: 250, editable: true },
    ],
    rows: data
  };
  const columns = React.useMemo(
    () => dataTable.columns.filter((column) => VISIBLE_FIELDS.includes(column.field)),
    [dataTable.columns]
  );
  return { dataTable, columns };
}
