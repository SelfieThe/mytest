import * as React from 'react';
import Box from '@mui/material/Box';
import { Button, Grid, TextField } from '@mui/material';

export function ToolBar(ControllerUser: { userName: string; handleChangeuserName: (event: React.ChangeEvent<HTMLInputElement>) => void; setUserName: React.Dispatch<React.SetStateAction<string>>; }, createUser: () => void, deleteSelectedFile: () => void) {
  return (
    <Box sx={{
      display: 'flex'
    }}>
      <Grid container item direction="row" style={{ display: 'flex', justifyContent: 'flex-start', paddingLeft: 5 }}>
        <TextField id="outlined-basic" label="User Name" variant="outlined" value={ControllerUser.userName} onChange={ControllerUser.handleChangeuserName} sx={{width: 160}}/>
        <Button onClick={createUser} variant="contained" disabled={ControllerUser.userName.length > 0 ? false : true} >Create</Button>
      </Grid>
      <Grid container item direction="row" style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button onClick={deleteSelectedFile} variant="contained" color="error">Delete</Button>
      </Grid>

    </Box>
  )
}
