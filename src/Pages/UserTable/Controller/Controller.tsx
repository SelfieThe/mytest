import React from "react";
import { GridSelectionModel } from '@mui/x-data-grid';

const validSymbols = new RegExp('[^a-zA-Z0-9]')

const ParametersLength = {
    userName: 16
}

export function Controller() { 
  const [userName, setUserName] = React.useState<string>('');
  const handleChangeuserName = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.target.value.length <= ParametersLength.userName ?
      validSymbols.test(event.target.value) ?
        null : setUserName(event.target.value)
      : null;
  };
  const ControllerUserName = { userName, handleChangeuserName, setUserName };
  return {ControllerUserName}
}

export function EditUser(data: { id: string; UserName: string; }[]) {
  return (e: any) => {
    const result: { id: string; UserName: string; } | any = data.find((item) => item.id === e.id);
    const updUser = { id: result.id, UserName: e.value };
    const foundIndex = data.findIndex((element) => element.id == result.id);
    data[foundIndex].id = updUser.id;
    data[foundIndex].UserName = updUser.UserName;
    localStorage.setItem('Table', JSON.stringify(data));
  };
}

export function CreateUser(ControllerUser: { userName: string; handleChangeuserName: (event: React.ChangeEvent<HTMLInputElement>) => void; setUserName: React.Dispatch<React.SetStateAction<string>>; }, setData: React.Dispatch<React.SetStateAction<{ id: string; UserName: string; }[]>>, data: { id: string; UserName: string; }[]) {
  return () => {
    const nevUser = { id: Math.random().toString(16).slice(-4), UserName: ControllerUser.userName };
    setData([...data, nevUser]);
    ControllerUser.setUserName('');
  };
}

export function DeleteUser(selectionRows: GridSelectionModel, data: { id: string; UserName: string; }[], setData: React.Dispatch<React.SetStateAction<{ id: string; UserName: string; }[]>>) {
  return () => {
    const selectedRows = new Set(selectionRows);
    if (data.length > 0)
      setData((row) => row.filter((element) => !selectedRows.has(element.id)));
    if (data.length === 1 || selectedRows.size === data.length)
      localStorage.setItem('Table', '[]');
  };
}