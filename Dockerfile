# base image
FROM node:12.18.3-alpine

# set working directory
WORKDIR /code

# add `/code/node_modules/.bin` to $PATH
ENV PATH /code/node_modules/.bin:$PATH

COPY . /code/
# install and cache app dependencies
#COPY package.json /code/package.json
#RUN npm update
RUN npm install --force --silent


# start app
CMD ["npm", "start"]
